package com.scriptor.api.controller;

import com.jaunt.JauntException;
import com.scriptor.api.service.ScriptService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class ScriptController {
    public final ScriptService scriptService;

    public ScriptController(ScriptService scriptService) {
        this.scriptService = scriptService;
    }

    @GetMapping("/script/{movieName}")
    public String getScripts(@PathVariable String movieName) throws JauntException {
        log.info("searched " + movieName);
        return scriptService.getScripts(movieName);
    }
}
