package com.scriptor.api.service;

import com.jaunt.JauntException;
import com.jaunt.NotFound;
import com.jaunt.ResponseException;
import com.jaunt.UserAgent;
import org.springframework.beans.factory.annotation.Lookup;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class ScriptService {
    @Value("${imsdb.url}")
    private String url;

    @Lookup
    @SuppressWarnings("SameReturnValue") //autoresolved by Spring @Lookup
    public UserAgent getUserAgent() {
        return null;
    }

    public String getScripts(String movieName) throws JauntException {
        UserAgent userAgent = getUserAgent();
        accessSearchMoviePage(movieName, userAgent);
        accessFirstFoundMoviePage(userAgent);
        accessScriptPage(userAgent);
        return userAgent.doc.findFirst("<td class=\"scrtext\">").getTextContent();

    }

    private void accessSearchMoviePage(String movieName, UserAgent userAgent) throws JauntException {
        userAgent.visit(url);
        userAgent.doc.apply(movieName).submit();
    }

    private void accessFirstFoundMoviePage(UserAgent userAgent) throws NotFound, ResponseException {
        String movieUrl = userAgent.doc.findEvery("<td valign=\"top\">").findFirst("<p>").findFirst("<a>").getAt("href");
        movieUrl = decodeSpacesFromUrl(movieUrl);
        userAgent.visit(movieUrl);
    }

    private void accessScriptPage(UserAgent userAgent) throws NotFound, ResponseException {
        String scripUrl = userAgent.doc.findEvery("<td valign=\"top\">").findFirst("<a>Read").getAt("href");
        scripUrl = decodeSpacesFromUrl(scripUrl);
        userAgent.visit(scripUrl);
    }

    private String decodeSpacesFromUrl(String scripUrl) {
        return scripUrl.replace(" ", "+");
    }
}
