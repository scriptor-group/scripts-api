package com.scriptor.api.config;

import com.jaunt.UserAgent;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class StartupBeans {
    @Bean
    @Scope(BeanDefinition.SCOPE_PROTOTYPE)
    public UserAgent userAgent() {
        return new UserAgent();
    }
}
