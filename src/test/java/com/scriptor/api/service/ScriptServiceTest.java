package com.scriptor.api.service;

import com.jaunt.Document;
import com.jaunt.Elements;
import com.jaunt.JauntException;
import com.jaunt.UserAgent;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.internal.util.reflection.Whitebox;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ScriptServiceTest {
    public static final String MOVIE_NAME = "Batman";
    public static final String URL = "https://www.imsdb.com/search.php";
    public static final String SEARCH_URL = "/search Url";
    public static final String SEARCH_URL_DECODED = "/search+Url";
    public static final String SCRIPT_URL = "/script Url";
    public static final String SCRIPT_URL_DECODED = "/script+Url";
    public static final String SCRIPTS = "scripts";

    @Spy
    @InjectMocks
    private ScriptService scriptService;

    @Mock
    private UserAgent userAgent;

    @Test
    void getScripts_success(@Mock Document document, @Mock Elements firstSearchElements) throws JauntException {
        when(scriptService.getUserAgent()).thenReturn(userAgent);

        Whitebox.setInternalState(userAgent, "doc", document);
        Whitebox.setInternalState(scriptService, "url", URL);

        //accessSearchMoviePage
        when(document.apply(MOVIE_NAME)).thenReturn(document);
        //accessFirstFoundMoviePage
        when(document.findEvery("<td valign=\"top\">"))
                .thenReturn(firstSearchElements) //accessFirstFoundMoviePage
                .thenReturn(firstSearchElements); //accessScriptPage
        when(firstSearchElements.findFirst("<p>")).thenReturn(firstSearchElements);
        when(firstSearchElements.findFirst("<a>")).thenReturn(firstSearchElements);
        when(firstSearchElements.getAt("href"))
                .thenReturn(SEARCH_URL) // accessFirstFoundMoviePage
                .thenReturn(SCRIPT_URL); //accessScriptPage
        //accessScriptPage
        when(firstSearchElements.findFirst("<a>Read")).thenReturn(firstSearchElements);
        //getScriptContent
        when(document.findFirst("<td class=\"scrtext\">")).thenReturn(firstSearchElements);
        when(firstSearchElements.getTextContent()).thenReturn(SCRIPTS);

        String actualScripts = scriptService.getScripts(MOVIE_NAME);

        assertEquals(SCRIPTS, actualScripts);
        ArgumentCaptor<String> urlsArgumentsCaptor = ArgumentCaptor.forClass(String.class);

        verify(userAgent, times(3)).visit(urlsArgumentsCaptor.capture());
        assertEquals(Arrays.asList(URL, SEARCH_URL_DECODED, SCRIPT_URL_DECODED), urlsArgumentsCaptor.getAllValues());

        verify(document).apply(MOVIE_NAME);

    }

}
