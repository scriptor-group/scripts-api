package com.scriptor.api.integrations;

import com.jaunt.UserAgent;
import com.scriptor.api.service.ScriptService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertNotSame;

@SpringBootTest
class StartupPrototypeBeanTest {

    @Autowired
    private ScriptService scriptService;

    @Test
    void getUserAgent_checkUserAgentIsPrototype_success() {
        UserAgent userAgent = scriptService.getUserAgent();
        UserAgent userAgent2 = scriptService.getUserAgent();
        assertNotSame(userAgent, userAgent2);
    }
}
