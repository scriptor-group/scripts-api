FROM maven:3.6.1-jdk-8 AS build
COPY src /usr/src/app/src
COPY pom.xml /usr/src/app
RUN mvn -f /usr/src/app/pom.xml clean package -DoutputDirectory=/usr/src/app

#FROM alpine:3.7
FROM openjdk:8-jdk-alpine
COPY --from=build /usr/src/app/target/scripts-api-0.0.1-SNAPSHOT.jar /usr/app/app.jar
EXPOSE 8080 5005
ENV JAVA_TOOL_OPTIONS -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=5005
ENTRYPOINT ["java","-jar","/usr/app/app.jar"]

#docker run -i -t _ /bin/sh
